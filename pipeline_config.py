import os
from selenium import webdriver

username = os.getenv("BROWSERSTACK_USERNAME")
access_key = os.getenv("BROWSERSTACK_ACCESS_KEY")

caps = {
    "os" : "Windows",
    "os_version" : "10",
    "browser" : "Chrome",
    "browser_version" : "latest",
    "resolution" : "2048x1536",
    "browserstack.local" : "false",
    "browserstack.debug" : "true",
    "browserstack.selenium_version" : "3.14.0",
    'browserstack.user': username,
    'browserstack.key': access_key
}

# driver = webdriver.Remote(
#     command_executor='https://hub-cloud.browserstack.com/wd/hub',
#     desired_capabilities=caps)