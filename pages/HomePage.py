from pages.UserInteraction import UserInteraction
from pages.BasePage import BasePage
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from pages.Locator import Locator


class HomePage(BasePage):

    url = "https://www.cellebrite.com/en/home/"

    @property
    def searchMagnifierBtn(self):
        locator = Locator(by=By.XPATH, value='//*[@id="h_search"]')
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def searchInput(self):
        locator = Locator(
            by=By.XPATH, value='//div[@id="srch_wrap"]//input[@id="h_search_input"]'
        )
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def searchBtn(self):
        locator = Locator(
            by=By.XPATH, value='//div[@id="srch_wrap"]//input[@id="searchsubmit"]'
        )
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def searchResultsForText(self):
        locator = Locator(by=By.XPATH, value='//h2[@class="scl-h2"]')
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def firstElementDescripion(self):
        locator = Locator(by=By.XPATH, value='(//li[@class="scl-item  "])[1]//h3')
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def nothingMatchedDescription(self):
        locator = Locator(by=By.XPATH, value='//div[@class="msg-bold"]')
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def searchFromSearchPageInput(self):
        locator = Locator(
            by=By.XPATH,
            value='//div[@class="srch-page-form n-found"]//input[@id="h_search_input"]',
        )
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def searchFromSearchPageBtn(self):
        locator = Locator(
            by=By.XPATH,
            value='//div[@class="srch-page-form n-found"]//input[@id="searchsubmit"]',
        )
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def menuBtn(self):
        locator = Locator(
            by=By.XPATH, value='//a[@data-shiftnav-target="shiftnav-main"]'
        )
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def languageDropDown(self):
        locator = Locator(by=By.XPATH, value='//div[@class="lang-switch"]//div')
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def spanishBtn(self):
        locator = Locator(
            by=By.XPATH, value='//ul[@class="l-list"]//a[@hreflang="es-ES"]'
        )
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def frenchBtn(self):
        locator = Locator(
            by=By.XPATH, value='//ul[@class="l-list"]//a[@hreflang="fr-FR"]'
        )
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def germanBtn(self):
        locator = Locator(
            by=By.XPATH, value='//ul[@class="l-list"]//a[@hreflang="de-DE"]'
        )
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def portugesseBtn(self):
        locator = Locator(
            by=By.XPATH, value='//ul[@class="l-list"]//a[@hreflang="pt-PT"]'
        )
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def englishBtn(self):
        locator = Locator(
            by=By.XPATH, value='//ul[@class="l-list"]//a[@hreflang="en-US"]'
        )
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def searchText(self):
        locator = Locator(by=By.XPATH, value='//div[@class="h-top-nav"]//a')
        return UserInteraction(driver=self.driver, locator=locator)

    @property
    def getStartedBtn(self):
        locator = Locator(by=By.XPATH, value='//div[@class="pm-0 single prima-cta"]//a')
        return UserInteraction(driver=self.driver, locator=locator)
