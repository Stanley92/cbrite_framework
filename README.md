## Setup

1. Install python to your local machine. (Check that it works: "python --version")
2. Check that you have installed pip (pip --version)
3. Install pipenv (pip install --user pipenv)
4. Run "pipenv install" from root directory to install all required packages.

## Run tests

1. Run all tests: "pytest -v"
2. Run api tests: "pytest -m "api" -v"
3. Run language tests: "pytest -m "language" -v"
4. Run search tests: "pytest -m "search" -v"

