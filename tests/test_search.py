import unittest
from selenium import webdriver
from pytest import mark
from pytest import fixture
from pages.UserInteraction import UserInteraction
from pages.HomePage import HomePage
from pages.BasePage import BasePage
from webdriver_manager.chrome import ChromeDriverManager
import time
# from pipeline_config import driver


@mark.search
class SearchTests(unittest.TestCase, BasePage):
    def setUp(self):
        self.basePage = BasePage(driver=SearchTests.browser)
        self.page = HomePage(driver=SearchTests.browser)
        self.page.go()

    @classmethod
    def setUpClass(cls):
        super(SearchTests, cls).setUpClass()
        cls.browser = webdriver.Chrome(ChromeDriverManager().install())

    @classmethod
    def tearDownClass(cls):
        super(SearchTests, cls).tearDownClass()
        cls.browser.quit()

    def search(self, text):
        self.page.searchMagnifierBtn.click
        self.page.searchInput.input_text(text)
        self.page.searchBtn.click

    def test_search_normal_input(self):
        self.search("security")

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/en/?s=security" in url

        actual = self.page.searchResultsForText.text
        actual2 = self.page.firstElementDescripion.text
        assert actual == "Search Results For: security"
        assert "security" in actual2

    def test_search_two_words(self):
        self.search("new capabilities")

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/en/?s=new+capabilities" in url

        actual = self.page.searchResultsForText.text
        actual2 = self.page.firstElementDescripion.text
        assert actual == "Search Results For: new capabilities"
        assert "New" in actual2
        assert "Capabilities" in actual2

    def test_search_special_character(self):
        self.search("%")

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/en/?s=%" in url

        actual = self.page.searchResultsForText.text
        actual2 = self.page.firstElementDescripion.text
        assert actual == "Search Results For: %"
        assert "%" in actual2

    def test_search_nothing_found(self):
        self.search("%^&$#")

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/en/?s=%25%5E%26%24%23" in url

        actual = self.page.searchResultsForText.text
        actual2 = self.page.nothingMatchedDescription.text
        assert actual == "Search Results For: %^&$#"
        expected = """Sorry, but nothing matched your search criteria. Please try again with different keywords."""
        assert expected in actual2

    def test_search_from_the_search_page(self):
        self.search("%^&$#")

        url = self.basePage.getUrl
        assert "/en/?s=%25%5E%26%24%23" in url

        self.page.searchFromSearchPageInput.moveToElement()
        self.page.searchFromSearchPageInput.clearText
        self.page.searchFromSearchPageInput.input_text("analysis")
        self.page.searchFromSearchPageBtn.click

        actual = self.page.searchResultsForText.text
        actual2 = self.page.firstElementDescripion.text
        assert actual == "Search Results For: analysis"
        assert "Analysis" in actual2

    def test_search_for_nothing(self):
        self.search("")

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/en/?s=" in url

        actual = self.page.searchResultsForText.text
        assert actual == "Search Results For:"

    def test_search_very_long_text(self):
        long_text = """You can spell check very long text areas without compromising any performance hits. Regardless of the size of the text, UltimateSpell only sends small portions of the text to the server as needed, while the user spell checks through the text. Basically the spell check dialog box makes on-demand calls to a callback page on the server without refreshing the whole page or dialog. It keeps processing small blocks of text using the AJAX techniques. Note that UltimateSpell displays the text in the dialog box sentence-by-sentence just like Microsoft Word. This helps the user understand the actual context in which the spelling error occurs."""

        self.search(long_text)

        actual = self.page.searchResultsForText.text
        assert actual == "Search Results For: " + long_text
