import pytest
import requests
import json

pytestmark = pytest.mark.api

base_url = "https://pacific-taiga-76065.herokuapp.com"

@pytest.mark.skip(msg= 'should return 200, 404 is returned ')
def test_delete_all_messages() -> None: 

    # delete all messages
    url = "%s/%s" % (base_url, "clear")

    result = requests.delete(
        url,
        headers={"x-authentication-token": "very-secrete-token"},
        verify=False,
    )
    assert result.status_code == 200

    # check that no messages are available
    url2 = "%s/%s" % (base_url, "messages")
    result2 = requests.get(
        url2,
        headers={"x-authentication-token": "very-secrete-token"},
        verify=False,
    )
    result2 = result2.status_code == 200
    assert result2 == True

def test_get_all_messages() -> None:
    url = "%s/%s" % (base_url, "messages")
    result = requests.get(
        url,
        headers={"x-authentication-token": "very-secrete-token"},
        verify=False,
    )
    assert result.status_code == 200

def test_get_all_users() -> None:
    url = "%s/%s" % (base_url, "users")

    result = requests.get(
        url,
        headers={"x-authentication-token": "very-secrete-token"},
        verify=False,
    )

    # expected = {
    #     "users":[
    #         {
    #             "id":"c787944f-28f0-53e0-b57f-488b685331a7",
    #             "age":62,
    #             "name":"Emma Scott",
    #             "last":"Cummings",
    #             "gender":"Male"
    #         },
    #         {
    #             "id":"50abdb2e-8634-5750-9b25-3f34cd9b9e0b",
    #             "age":29,
    #             "name":"Joe Smith",
    #             "last":"Puccini",
    #             "gender":"Male"
    #         },
    #         {
    #             "id":"e6beab14-dfeb-50ab-8139-6e5062681406",
    #             "age":27,
    #             "name":"Peter Hughes",
    #             "last":"Perry",
    #             "gender":"Male"
    #         },
    #         {
    #             "id":"e69d5b4e-1385-59f9-be0c-8cd9d9ca9464",
    #             "age":31,
    #             "name":"Margaret Fisher",
    #             "last":"van Schaik",
    #             "gender":"Male"
    #         },
    #         {
    #             "id":"f5f1547c-a95f-5a91-8933-e3c3df2fe8e5",
    #             "age":47,
    #             "name":"Vera Harvey",
    #             "last":"Chandler",
    #             "gender":"Female"
    #         }
    #     ]
    # }

    assert result.status_code == 200
    # assert result.json() == expected
    # assert result.json() == expected

def test_post_message() -> None:
    userId = 'adf8d8c6-364d-5fd7-bba4-22faa1bb7c17'
    url = "%s/%s/%s/%s" % (base_url, "messages", userId, 'message')

    payload = {
         "message":"Some message",
         "user":{
            "id":"adf8d8c6-364d-5fd7-bba4-22faa1bb7c17",
            "age":34,
            "name":"Betty Garner",
            "last":"Howells",
            "gender":"Female"
         }
      },

    result = requests.post(
        url,
        data=payload,
        headers={"x-authentication-token": "very-secrete-token"},
        verify=False,
    )

    assert result.status_code == 200 or result.status_code == 201

@pytest.mark.skip(msg= 'should return 200, 404 is returned ')
def test_get_one_user() -> None:
    userId = 'e6beab14-dfeb-50ab-8139-6e5062681406'
    url = "%s/%s/%s" % (base_url, "users", userId)

    result = requests.get(
        url,
        headers={"x-authentication-token": "very-secrete-token"},
        verify=False,
    )

    assert result.status_code == 200