import unittest
from selenium import webdriver
from pytest import mark
from pytest import fixture
from pages.UserInteraction import UserInteraction
from pages.HomePage import HomePage
from pages.BasePage import BasePage
from webdriver_manager.chrome import ChromeDriverManager
import time
# from pipeline_config import driver


@mark.language
class LanguageTests(unittest.TestCase, BasePage):
    def setUp(self):
        self.basePage = BasePage(driver=LanguageTests.browser)
        self.page = HomePage(driver=LanguageTests.browser)
        self.page.go()

    @classmethod
    def setUpClass(cls):
        super(LanguageTests, cls).setUpClass()
        cls.browser = webdriver.Chrome(ChromeDriverManager().install())

    @classmethod
    def tearDownClass(cls):
        super(LanguageTests, cls).tearDownClass()
        cls.browser.quit()

    def test_language_spanish(self):
        self.page.languageDropDown.click
        self.page.spanishBtn.click

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/es/pagina-principal/" == url

        actual = self.page.searchText.text
        expected = "Formación"
        assert actual in expected

        actual2 = self.page.getStartedBtn.text
        expected2 = "Empezar"
        assert actual2 in expected2

    def test_language_french(self):
        self.page.languageDropDown.click
        self.page.frenchBtn.click

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/fr/accueil/" == url

        actual = self.page.searchText.text
        expected = "formation"
        assert actual in expected

        actual2 = self.page.getStartedBtn.text
        expected2 = "Commencer"
        assert actual2 in expected2

    def test_language_german(self):
        self.page.languageDropDown.click
        self.page.germanBtn.click

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/de/startseite/" == url

        actual = self.page.searchText.text
        expected = "Training"
        assert actual in expected

        actual2 = self.page.getStartedBtn.text
        expected2 = "Erste Schritte"
        assert actual2 in expected2

    def test_language_portugesse(self):
        self.page.languageDropDown.click
        self.page.portugesseBtn.click

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/pt/inicio/" == url

        actual = self.page.searchText.text
        expected = "Treinamento"
        assert actual in expected

        actual2 = self.page.getStartedBtn.text
        expected2 = "Começar"
        assert actual2 in expected2

    def test_change_language_from_another_language(self):
        self.page.languageDropDown.click
        self.page.portugesseBtn.click

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/pt/inicio/" == url

        self.page.languageDropDown.click
        self.page.englishBtn.click

        url = self.basePage.getUrl
        assert "https://www.cellebrite.com/en/home/" == url

        actual = self.page.searchText.text
        expected = "Blog"
        assert actual in expected

        actual2 = self.page.getStartedBtn.text
        expected2 = "Get Started"
        assert actual2 in expected2
